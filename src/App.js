import './App.css';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Calculetor from './componate/calculetor';

function App() {
  return (
    <>
 <Router>
  <Routes>
    <Route path='/' element={<Calculetor/>}/>
  </Routes>
 </Router>

    </>
  );
}

export default App;
