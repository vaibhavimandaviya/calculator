import React, { useState } from "react";
import "../App.css";

const Calculetor = () => {
  const [result, setResult] = useState("");
  const [sign, setSign] = useState("");
  const [result2, setResult2] = useState("");
  const [total, setTotal] = useState("");

  const Sign = (e) => {
    if (sign.length == 1) {
      setSign(e);
    } else {
      setSign(e);
    }
  };

  const Result = (e) => {
    if (result.length < 12) {
      setResult2(result2.concat(e.target.name));
      // setResult2.replace(/\d/g, result)
    }
  };

  const handleclick = (e) => {
    if (result.length < 12) {
      setResult(result.concat(e.target.name));
    }
  };

  const backspace = () => {
    setResult(result.slice(0, -1))
    setResult2(result2.slice(0, -1));
  };

  const clear = () => {
    setResult("");
    setTotal("");
    setResult2("");
    setSign("");
  };

  const calculate = () => {
    setSign("=");
    try {
      setTotal(eval(result + sign + result2).toString());
    } catch (error) {
      setResult("Error");
    }
  };

  return (
    <>
      <div className=" grid justify-center items-center mt-16 md:mt-0">
        <p className=" text-[#f8ccc4] text-center text-4xl my-12 uppercase ">
          calculator
        </p>
        <div className="conatine shadow-sm shadow-slate-400 p-5 bg-[#111111] rounded-lg h-[33rem] w-80 md:h-[30rem] md:w-auto">
          <form>
            <input
              type="text"
              value={sign == "=" ? total : sign.length == 1 ? result2 : result}
              placeholder="0"
              className="shadow-inner shadow-[#757575] bg-[#262834] rounded-md p-2 mb-4 md:w-[249px] md:h-[75px] h-20 w-[17.5rem] overflow-hidden"
            />
          </form>
          <div className="keypad my-6">
            <button
              className="highlight shadow-md shadow-gray-600 mb-3 h-[3.5rem] md:h-12 font-bold"
              onClick={clear}
              id="clear"
            >
              CL
            </button>
            <button
              className="highlight text-black shadow-md shadow-gray-600 mb-3 h-[3.5rem] md:h-12 font-bold"
              onClick={backspace}
              id="backspace"
            >
              DL
            </button>
            <button
              className={` shadow-md shadow-gray-600 mb-3 h-[3.5rem] md:h-12 font-bold ${
                sign == "/" ? "bg-black" : "bg-[#f8ccc4] text-black"
              }`}
              name="/"
              onClick={() => {
                Sign("/");
              }}
            >
              &divide;
            </button>
            <button
              className={`shadow-md shadow-gray-600 mb-3 h-[3.5rem] mx-2 md:h-12 `}
              name="7"
              onClick={sign.length == 1 ? Result : handleclick}
            >
              7
            </button>
            <button
              className="shadow-md shadow-gray-600 mb-3 h-[3.5rem] mx-2 md:h-12"
              name="8"
              onClick={sign.length == 1 ? Result : handleclick}
            >
              8
            </button>
            <button
              className="shadow-md shadow-gray-600 mb-3 h-[3.5rem] mx-2 md:h-12"
              name="9"
              onClick={sign.length == 1 ? Result : handleclick}
            >
              9
            </button>
            <button
              className={` shadow-md shadow-gray-600 mb-3 h-[3.5rem] md:h-12 font-bold ${
                sign == "*" ? "bg-black" : "bg-[#f8ccc4] text-black"
              }`}
              name="*"
              onClick={() => {
                Sign("*");
              }}
            >
              &times;
            </button>
            <button
              className="shadow-md shadow-gray-600 mb-3 h-[3.5rem] mx-2 md:h-12"
              name="4"
              onClick={sign.length == 1 ? Result : handleclick}
            >
              4
            </button>
            <button
              className="shadow-md shadow-gray-600 mb-3 h-[3.5rem] mx-2 md:h-12"
              name="5"
              onClick={sign.length == 1 ? Result : handleclick}
            >
              5
            </button>
            <button
              className="shadow-md shadow-gray-600 mb-3 h-[3.5rem] mx-2 md:h-12"
              name="6"
              onClick={sign.length == 1 ? Result : handleclick}
            >
              6
            </button>
            <button
              className={` shadow-md shadow-gray-600 mb-3 h-[3.5rem] md:h-12 font-bold ${
                sign == "-" ? "bg-black" : "bg-[#f8ccc4] text-black"
              }`}
              name="-"
              onClick={() => {
                Sign("-");
              }}
            >
              &ndash;
            </button>
            <button
              className="shadow-md shadow-gray-600 mb-3 h-[3.5rem] mx-2 md:h-12"
              name="1"
              onClick={sign.length == 1 ? Result : handleclick}
            >
              1
            </button>
            <button
              className="shadow-md shadow-gray-600 mb-3 h-[3.5rem] mx-2 md:h-12"
              name="2"
              onClick={sign.length == 1 ? Result : handleclick}
            >
              2
            </button>
            <button
              className="shadow-md shadow-gray-600 mb-3 h-[3.5rem] mx-2 md:h-12"
              name="3"
              onClick={sign.length == 1 ? Result : handleclick}
            >
              3
            </button>
            <button
              className={` shadow-md shadow-gray-600 mb-3 h-[3.5rem] md:h-12 font-bold ${
                sign == "+" ? "bg-black" : "bg-[#f8ccc4] text-black"
              }`}
              name="+"
              onClick={() => {
                Sign("+");
              }}
            >
              +
            </button>
            <button
              className="shadow-md shadow-gray-600 mb-3 h-[3.5rem] mx-2 md:h-12"
              name="0"
              onClick={sign.length == 1 ? Result : handleclick}
            >
              0
            </button>
            <button
              className="shadow-md shadow-gray-600 mb-3 h-[3.5rem] mx-2 md:h-12"
              name="."
              onClick={sign.length == 1 ? Result : handleclick}
            >
              .
            </button>
            <button
              className={` shadow-md shadow-gray-600 mb-3 h-[3.5rem] md:h-12 font-bold ${
                sign == "=" ? "bg-black" : "bg-[#f8ccc4] text-black"
              }`}
              onClick={calculate}
              id="result"
            >
              =
            </button>
          </div>
        </div>
      </div>
    </>
  );
};

export default Calculetor;
